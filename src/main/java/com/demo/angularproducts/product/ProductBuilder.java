package com.demo.angularproducts.product;

/**
 * Created by Admin on 14.07.2017.
 */
public final class ProductBuilder
{
    private String descritpion;
    private String price;
    private String name;

    private ProductBuilder()
    {
    }

    public static ProductBuilder aProduct()
    {
        return new ProductBuilder();
    }

    public ProductBuilder withDescritpion(String descritpion)
    {
        this.descritpion = descritpion;
        return this;
    }

    public ProductBuilder withPrice(String price)
    {
        this.price = price;
        return this;
    }

    public ProductBuilder withName(String name)
    {
        this.name = name;
        return this;
    }

    public Product build()
    {
        Product product = new Product();
        product.setDescription(descritpion);
        product.setPrice(price);
        product.setName(name);
        return product;
    }
}
