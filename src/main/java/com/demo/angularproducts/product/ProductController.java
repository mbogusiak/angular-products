
package com.demo.angularproducts.product;

import com.demo.angularproducts.product.dto.ProductBasicDto;
import com.demo.angularproducts.product.dto.ProductConverter;
import com.demo.angularproducts.product.dto.ProductFullDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("products")
public class ProductController
{
    private final ProductService productService;
    private final ProductConverter productConverter;

    public ProductController(ProductService productService, ProductConverter productConverter)
    {
        this.productService = productService;
        this.productConverter = productConverter;
    }

    @GetMapping
    public List<ProductFullDto> getProducts()
    {
        return productConverter.getProducts(productService.findAll());
    }

    @GetMapping("/{id}")
    public ProductFullDto getProduct(@PathVariable Long id)
    {
        return productConverter.getProduct(productService.findProduct(id));
    }
}
