package com.demo.angularproducts.product.dto;


import com.demo.angularproducts.product.Product;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductConverter
{
    private MapperFacade mapper;

    public List<ProductFullDto> getProducts(List<Product> products){
        return mapper.mapAsList(products, ProductFullDto.class);
    }


    public ProductFullDto getProduct(Product product){
        return mapper.map(product, ProductFullDto.class);
    }
    @Autowired
    public void setMapperFactory(MapperFactory mapperFactory) {
        this.mapper = mapperFactory.getMapperFacade();
    }


}
