package com.demo.angularproducts.product.dto;

public class ProductBasicDto
{
    private Long id;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
