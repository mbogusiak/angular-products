package com.demo.angularproducts.product.dto;

import com.demo.angularproducts.product.attribute.dto.AttributeDto;

import java.util.List;

public class ProductFullDto
{
    private Long id;
    private String description;
    private String price;
    private String name;
    private String image;
    List<AttributeDto> attributes;
    
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public List<AttributeDto> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(List<AttributeDto> attributes)
    {
        this.attributes = attributes;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
