package com.demo.angularproducts.product;

import com.demo.angularproducts.product.attribute.Attribute;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Product{
    @Id
    @GeneratedValue
    private Long id;
    private String description;
    private String price;
    private String name;
    private String image;
    @OneToMany
    List<Attribute> attributes;

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Attribute> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes)
    {
        this.attributes = attributes;
    }

    public Long getId()
    {
        return id;
    }

}

