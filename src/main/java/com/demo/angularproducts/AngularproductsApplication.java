package com.demo.angularproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularproductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularproductsApplication.class, args);
	}
}
