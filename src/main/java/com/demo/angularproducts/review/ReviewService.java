package com.demo.angularproducts.review;

import com.demo.angularproducts.product.Product;
import com.demo.angularproducts.product.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ReviewService
{
    private final ReviewRepository reviewRepository;
    private final ProductService productService;

    public ReviewService(ReviewRepository reviewRepository, ProductService productService)
    {
        this.reviewRepository = reviewRepository;
        this.productService = productService;
    }

    @Transactional
    public void creteReview(Review review, Long productID)
    {
        Product product = productService.findProduct(productID);
        review.setProduct(product);
        reviewRepository.save(review);
    }

    public List<Review> findByProduct(Long productID)
    {
        return reviewRepository.findByProduct(productService.findProduct(productID));
    }
}