package com.demo.angularproducts.review;

import com.demo.angularproducts.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ReviewRepository extends JpaRepository<Review, Long>
{
    List<Review> findByProduct(Product product);
}
