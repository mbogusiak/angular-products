package com.demo.angularproducts.review;

import com.demo.angularproducts.product.Product;

import javax.persistence.*;

@Entity
public class Review
{
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private Product product;
    private String description;
    @Column(length = 1024)
    private String content;

    public Product getProduct()
    {
        return product;
    }

    public void setProduct(Product product)
    {
        this.product = product;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
