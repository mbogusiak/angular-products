package com.demo.angularproducts.review;

import com.demo.angularproducts.review.dto.ReviewConverter;
import com.demo.angularproducts.review.dto.ReviewBasicDto;
import com.demo.angularproducts.review.dto.ReviewFullDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reviews/products/{productID}")
@CrossOrigin
public class ReviewController
{
    private final ReviewConverter reviewConverter;
    private final ReviewService reviewService;
    public ReviewController(ReviewConverter reviewConverter, ReviewService reviewService)
    {
        this.reviewConverter = reviewConverter;
        this.reviewService = reviewService;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void createReview(@RequestBody ReviewFullDto review,@PathVariable Long productID)
    {
        reviewService.creteReview(reviewConverter.getReview(review),productID);
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<ReviewBasicDto> getReviews(@PathVariable Long productID)
    {
       return  reviewConverter.getReviews(reviewService.findByProduct(productID));
    }



}
