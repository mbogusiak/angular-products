package com.demo.angularproducts.review.dto;

import com.demo.angularproducts.review.Review;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReviewConverter
{

    private MapperFacade mapper;

    public List<ReviewBasicDto> getReviews(List<Review> reviews){
        return mapper.mapAsList(reviews, ReviewBasicDto.class);
    }

    public Review getReview(ReviewFullDto review){
        return mapper.map(review, Review.class);
    }
    @Autowired
    public void setMapperFactory(MapperFactory mapperFactory) {
        this.mapper = mapperFactory.getMapperFacade();
    }
}
