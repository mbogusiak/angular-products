package com.demo.angularproducts.review.dto;

import com.demo.angularproducts.product.dto.ProductBasicDto;

public class ReviewFullDto
{
    private ProductBasicDto product;
    private String description;
    private String content;

    public ProductBasicDto getProduct()
    {
        return product;
    }

    public void setProduct(ProductBasicDto product)
    {
        this.product = product;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
