package com.demo.angularproducts.review.dto;

import com.demo.angularproducts.product.dto.ProductBasicDto;

public class ReviewBasicDto
{
    private String description;
    private String content;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
