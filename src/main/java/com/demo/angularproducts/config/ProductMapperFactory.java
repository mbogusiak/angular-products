package com.demo.angularproducts.config;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

@Component
public class ProductMapperFactory implements FactoryBean<MapperFactory>
{
    @Override
    public MapperFactory getObject() throws Exception
    {
        MapperFactory factory = new DefaultMapperFactory.Builder().mapNulls(false).build();
        return factory;
    }

    @Override
    public Class<?> getObjectType()
    {
        return MapperFactory.class;
    }

    @Override
    public boolean isSingleton()
    {
        return true;
    }
}
