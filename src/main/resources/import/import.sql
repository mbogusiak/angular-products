INSERT INTO Product (id,description,price,name,image) values(1000,'Sed ut perspiciatis unde omnis iste natu','$1,398.00','Sony DSCWX350','http://lorempixel.com/400/200/technics/1');

INSERT INTO Product (id,description,price,name,image) values(1001,'Magnam aliquam quaerat voluptatem','$1,298.00','YI 4K Camera ','http://lorempixel.com/400/200/technics/2');
INSERT INTO Product (id,description,price,name,image) values(1002,'Quia non numquam eius modi tempora','$1,191.00','Nikon Coolpix A900','http://lorempixel.com/400/200/technics/3');
INSERT INTO Product (id,description,price,name,image) values(1003,'Beatae vitae dicta sunt explicabo','$2,398.00','Sony Alpha a6000  Camera','http://lorempixel.com/400/200/technics/4');
INSERT INTO Product (id,description,price,name,image) values(1004,'Aspernatur aut odit aut fugit, sed','$3,592.00','Sony Alpha a6500','http://lorempixel.com/400/200/technics/5');
INSERT INTO Product (id,description,price,name,image) values(1005,'Nisi ut aliquid ex ea commodi consequatur','$1,798.00','YI Dome Camera Pan','http://lorempixel.com/400/200/technics/6');
INSERT INTO Product (id,description,price,name,image) values(1006,'Exercitation ullamco laboris nisi ut aliquip ','$2,352.00','Sony DSC-Cyber-shot','http://lorempixel.com/400/200/technics/7');
INSERT INTO Product (id,description,price,name,image) values(1007,'Excepteur sint occaecat cupidata','$4,198.00','Sony DSC-RX10 III ','http://lorempixel.com/400/200/technics/8');

INSERT INTO Review (id,product_id,description,content) values(2007,1000,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2008,1000,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2009,1000,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');

INSERT INTO Review (id,product_id,description,content) values(2010,1001,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2011,1001,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2012,1001,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');

INSERT INTO Review (id,product_id,description,content) values(2013,1002,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2014,1002,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2015,1002,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');

INSERT INTO Review (id,product_id,description,content) values(2016,1003,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2017,1003,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2018,1003,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');

INSERT INTO Review (id,product_id,description,content) values(2019,1004,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2021,1004,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2022,1004,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');

INSERT INTO Review (id,product_id,description,content) values(2023,1005,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2024,1005,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2025,1005,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');

INSERT INTO Review (id,product_id,description,content) values(2026,1006,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2027,1006,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2028,1006,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');

INSERT INTO Review (id,product_id,description,content) values(2029,1007,'This is nice product','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
INSERT INTO Review (id,product_id,description,content) values(2030,1007,'Great stuff!','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga');
INSERT INTO Review (id,product_id,description,content) values(2031,1007,'Dont buy it','Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella');


INSERT INTO Attribute (id,name,value) values(3000,'Auto Focus Technology','Multi-area');
INSERT INTO Attribute (id,name,value) values(3001,'Color','Black');
INSERT INTO Attribute (id,name,value) values(3002,'Compatible Mountings','Sony Alpha');
INSERT INTO Attribute (id,name,value) values(3003,'Continuous Shooting Speed','	10 fps');
INSERT INTO Attribute (id,name,value) values(3004,'Digital Zoom','40x');
INSERT INTO Attribute (id,name,value) values(3005,'Display','LCD');
INSERT INTO Attribute (id,name,value) values(3006,'Display Fixture Type','Fixed');
INSERT INTO Attribute (id,name,value) values(3007,'Display Resolution Maximum','460000');
INSERT INTO Attribute (id,name,value) values(3008,'Display Size','3 inches');
INSERT INTO Attribute (id,name,value) values(3009,'Item Weight','0.3 pounds');
INSERT INTO Attribute (id,name,value) values(3010,'Lens Type','zoom');
INSERT INTO Attribute (id,name,value) values(3011,'Lithium Battery Energy Content','2 Watt Hours');
INSERT INTO Attribute (id,name,value) values(3012,'Lithium Battery Voltage','	5 Volts');
INSERT INTO Attribute (id,name,value) values(3013,'Lithium Battery Weight','3.5 grams');
INSERT INTO Attribute (id,name,value) values(3014,'Manufacturer Warranty Description','1 year Manufacturer supplied Full USA Warranty');
INSERT INTO Attribute (id,name,value) values(3015,'Maximum Aperture','3.5');
INSERT INTO Attribute (id,name,value) values(3016,'Maximum Aperture Range','F3.5 - F6.5');
INSERT INTO Attribute (id,name,value) values(3017,'Maximum Focal Length','500 mm');
INSERT INTO Attribute (id,name,value) values(3018,'Maximum Shutter Speed','1/1600 of a second');
INSERT INTO Attribute (id,name,value) values(3019,'Maximum horizontal resolution','4,896');
INSERT INTO Attribute (id,name,value) values(3020,'Metering','Multi, Center-weighted, Spot');
INSERT INTO Attribute (id,name,value) values(3021,'Minimum Focal Length','25 mm');
INSERT INTO Attribute (id,name,value) values(3022,'Minimum Shutter Speed','4 seconds');
INSERT INTO Attribute (id,name,value) values(3023,'Optical Sensor Resolution','18 MP');
INSERT INTO Attribute (id,name,value) values(3024,'Optical Sensor Size','1/2.3_inches');
INSERT INTO Attribute (id,name,value) values(3025,'Photo Sensor Technology','BSI-CMOS');
INSERT INTO Attribute (id,name,value) values(3026,'Removable Memory','Secure Digital card');
INSERT INTO Attribute (id,name,value) values(3027,'Resolution modes','21.1MP');
INSERT INTO Attribute (id,name,value) values(3028,'Self-timer','Yes');
INSERT INTO Attribute (id,name,value) values(3029,'Shipping Weight','0.45 pounds');
INSERT INTO Attribute (id,name,value) values(3030,'Software Included','true');

INSERT INTO Product_Attributes(product_id,attributes_id) values(1000,3000);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1000,3001);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1000,3002);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1000,3027);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1000,3028);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1000,3029);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1000,3030);

INSERT INTO Product_Attributes(product_id,attributes_id) values(1001,3003);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1001,3004);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1001,3005);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1001,3024);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1001,3025);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1001,3026);

INSERT INTO Product_Attributes(product_id,attributes_id) values(1002,3006);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1002,3007);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1002,3008);

INSERT INTO Product_Attributes(product_id,attributes_id) values(1003,3009);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1003,3010);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1003,3011);

INSERT INTO Product_Attributes(product_id,attributes_id) values(1004,3012);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1004,3013);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1004,3014);

INSERT INTO Product_Attributes(product_id,attributes_id) values(1005,3015);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1005,3016);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1005,3017);

INSERT INTO Product_Attributes(product_id,attributes_id) values(1006,3018);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1006,3019);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1006,3020);

INSERT INTO Product_Attributes(product_id,attributes_id) values(1007,3021);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1007,3022);
INSERT INTO Product_Attributes(product_id,attributes_id) values(1007,3023);
